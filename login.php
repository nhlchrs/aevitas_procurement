<?php 
include $_SERVER['DOCUMENT_ROOT'].'/include/session.php';
include $_SERVER['DOCUMENT_ROOT'].'/core/db.php';
include $_SERVER['DOCUMENT_ROOT'].'/helper/helper.php';
if(isset($_SESSION['id'])){
    header("location:/");
} 
if(isset($_POST['email'])){
	$username = $_POST['email'];
	$password = $_POST['password'];
	$error_desc=NULL;
	$error_code=NULL;
	$output = Helper::logging_in($username, $password);
	if($output=='mismatch'){
		$error_desc = "Warning! Wrong Credentials";
		$error_code = '1';
	}
	elseif ($output=='error') {
		$error_desc="Warning! No user registered with this Mail or Username!";
		$error_code='2';
	}
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <link rel="shortcut icon" type="text/css" href="./images/apspl.jpg">
    <link rel="stylesheet" type="text/css" href="/css/fonts/font-awesome.css">
    <title>Login | Aevitas Procurement Service </title>
</head>
<body>
	<?php
	include $_SERVER['DOCUMENT_ROOT']."/include/navbar.first.php";
	include $_SERVER['DOCUMENT_ROOT']."/include/navbar.php"; ?>
	<br>
<div class="container">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3 col-center">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-6">
								<a href="login.php" class="active" id="login-form-link">Login</a>
							</div>
							<div class="col-6">
								<a href="register.php" id="">Register</a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="login-form"  method="post" role="form" style="display: block;">
									<div class="form-group">
										<input type="text" name="email" id="email" tabindex="1" class="form-control" placeholder="Enter Email / Username" required>
									</div>
									<div class="form-group">
										<input type="password" name="password" id="login-
										password" tabindex="2" class="form-control" placeholder="Password" required>
									</div>
									<div class="form-group text-center">
										<input type="checkbox" tabindex="3" class="" name="remember" id="remember">
										<label for="remember"> Remember Me</label>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In">
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-lg-12">
												<div class="text-center">
													<a href="recover.php" tabindex="5" class="forgot-password">Forgot Password?</a>
												</div>
											</div>
										</div>
									</div>
									<?php if(isset($error_code)&& $error_code=='1'&&$error_desc){echo '<div class="error-msg">'.$error_desc.'</div>';}?>
									<?php if(isset($error_code)&& $error_code=='2'&&$error_desc){echo '<div class="error-msg">'.$error_desc.'</div>';}?>
								</form>
								
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div><br><br>
</body>
<script type="text/javascript" src="./js/mine.js"></script>
<?php include $_SERVER['DOCUMENT_ROOT'].'/include/footer.php'; ?>
</html>
