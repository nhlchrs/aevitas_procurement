<?php 
include $_SERVER['DOCUMENT_ROOT']."/core/db.php";
include $_SERVER['DOCUMENT_ROOT']."/include/session.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" type="text/css" href="/css/suggestion-box.min.css">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/suggestion-box.min.js"></script>
    <link rel="shortcut icon" type="text/css" href="./images/apspl.jpg">
    <link rel="stylesheet" type="text/css" href="/css/fonts/font-awesome.css">
    
    <title>Aevitas Procurement Service </title>
</head>

<body>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/navbar.first.php";?>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/navbar.php"; ?>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-12 text-center">
            <div class="bgimg">
    <img src="./images/main.jpg" alt="body images of aevitas Procurement" class="" style="width: 100%;">
<h4 class="centered">WELCOME TO THE WORLD OF ONE VENDOR SOLUTION...</h4>  
</div>
        </div>
</div>
</div>
<div class="vendor">
    <h3 style="text-align: center; margin-top: 15px;">Our Authorize vendor</h3>
    <div class="vendor-detail">
        <marquee scrollamount="30" onmouseover="this.stop();" onmouseout="this.start();">
            <div class="image-speed" >
            <img src="./images/cello.jpeg" style="width: 200px; height: 100px; margin-left:40px;">
            <img src="./images/micromax.jpeg" style="width: 200px; height: 100px; margin-left:40px;">
            <img src="./images/nilkamal.jpeg" style="width: 200px; height: 100px; margin-left:40px;">
            <img src="./images/vester.jpeg" style="width: 200px; height: 100px; margin-left:40px;">
            <img src="./images/voltas.jpeg" style="width: 200px; height: 100px; margin-left:40px;">
            <img src="./images/whirlpool.jpeg" style="width: 200px; height: 100px; margin-left:40px;"    >
            </div>
        </marquee>
    </div>
</div>

<section class="clients text-center" >
    <h2 style="text-align: center;">Our Clients</h2>
    <div class="row rowsetting">
    <div class="col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto " style="">
        <img src="./images/mahindra.jpeg" style="width:200px; height: 200px">
        <h2>Mahindra & Mahindra Ltd</h2>
    </div>
    <div class=" col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto " style="">
        <img src="./images/krafthenz.jpeg" style="width:200px; height: 200px">
        <h2>Heinz India Pvt Ltd</h2>
    </div>
    <div class=" col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto " style="">
        <img src="./images/setco.jpeg" style="width:200px; height: 200px">
        <h2>SETCO AUTOMOTIVE LTD</h2>
    </div>
        
        
    </div>
    </section>
<section class="services bg-light">
    <div style="margin-top: 40px;">
    <div class="container text-center">
        <h2> Our services</h2>
        <div class="row ">
             <div class="holo col-lg-12 col-md-12 col-sm-12 col-10  m-auto " style="">
        <img src="./images/hologram.jpg" style="width:100%; height: 100%; margin-top:50px; margin-bottom: 50px; border-radius: 30px;">
    </div>
        </div>
    </div>
</div>

</section>
<div style="margin-top: 40px;"></div>
<section class="bg-light text-center" style="margin-bottom: 30px">
    <div style="margin-top: 40px;"></div>
    <h2 style="margin-bottom: 30px; margin-top: 50px;">How we can help you?</h2>
    <p class="text-head">Provide better platform to fulfill all your requirement from different Vendor. Our Team will also take care of Genuine Product Supply from Verified Vendor with a Hassle Free Service.</p>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-10  d-block m-auto " style="margin-top: 30px">
<div class="service-icon d-block m-auto"> <i class="fa fa-compass fa-4x" aria-hidden="true"></i>
<h4>Vision</h4>
<p class="text-p">Provide One Window Solution from different Verified Vendors for Genuine Products.</p>
</div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto " style="">
<div class="service-icon d-block m-auto" > <i class="fa fa-desktop fa-4x" style="margin-top: 30px;" aria-hidden="true"></i>
<h4>Development</h4>
<p class="text-p">We want to know what customers are looking for, what their values are, and how can we meet their needs. It’s not just about Big Data; it’s about translating that into the truth.</p>
</div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto " style="">
<div class="service-icon d-block m-auto"> <i class="fa fa-bullhorn fa-4x" style="margin-top: 30px;" aria-hidden="true"></i>
<h4>Marketing</h4>
<p class="text-p">“Innovation needs to be part of your culture. Consumers are transforming faster than we are, and if we don’t catch up, we’re in trouble.”</p>
</div>
        </div>
    </div>
    <div class="row" style="">
        <div class="col-lg-4 col-md-4 col-sm-4 col-10  d-block m-auto">
<div class="service-icon d-block m-auto"> <i class="fa fa-share-alt fa-4x" style="margin-top: 30px" aria-hidden="true"></i>
<h4>Social Media</h4>
<p class="text-p">You can get various updated and get in touch with us on our Linkedin Account.</p>
</div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto" >
<div class="service-icon d-block m-auto"> <i class="fa fa-cart-arrow-down fa-4x" style="margin-top: 30px"aria-hidden="true"></i>
<h4>eCommerce</h4>
<p class="text-p">In eCommerce, our prices are better because the consumer has to take a leap of faith in our product.</p>
</div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto" >
<div class="service-icon d-block m-auto"> <i class="fa fa-support fa-4x" style="margin-top: 30px" aria-hidden="true"></i>
<h4>Support</h4>
<p class="text-p">Can Contact us at on our E-mail as well as on our Helpline Number.</p>
</div>
        </div>
    </div>
</section>
<div style="margin-top: 40px;"></div>
<section class="product  text-center">
    <h2 style="margin-top: 50px; margin-bottom: 50px;">Our Products</h2>
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4 col-10  d-block m-auto " style="">
<div class="service-icon d-block m-auto"> <i class="fa fa-shopping-basket fa-4x"style="margin-top: 30px;" aria-hidden="true"></i>
<h4>Office Supplies</h4>
<button class="btn btn-outline-success">Read More</button>
</div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto " style="">
<div class="service-icon d-block mine-auto"> <i class="fa fa-car fa-4x" style="margin-top: 30px;" aria-hidden="true"></i>
<h4>Automotive Maintanance & Accessories</h4>
<button class="btn btn-outline-success">Read More</button>
</div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto " style="">
<div class="service-icon d-block m-auto"> <i class="fa fa-snowflake-o fa-4x" style="margin-top: 30px;" aria-hidden="true"></i>
<h4>Tools and Cutting</h4>
<button class="btn btn-outline-success">Read More</button>
</div>
        </div>
    </div>
    <div class="row" style= "margin-bottom: 50px;">
        <div class="col-lg-4 col-md-4 col-sm-4 col-10  d-block m-auto " style="">
<div class="service-icon d-block m-auto"> <i class="fa fa-television fa-4x" style="margin-top: 30px;" aria-hidden="true"></i>
<h4>IT Electronic</h4>
<button class="btn btn-outline-success">Read More</button>
</div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto " style="">
<div class="service-icon d-block m-auto"> <i class="fa fa-lightbulb-o fa-4x" style="margin-top: 30px;" aria-hidden="true"></i>
<h4>Led & Lights</h4>
<button class="btn btn-outline-success">Read More</button>
</div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto " style="">
<div class="service-icon d-block m-auto"> <i class="fa fa-hospital-o fa-4x" style="margin-top: 30px;" aria-hidden="true"></i>
<h4>Funiture Hospitality & Food Service</h4>
<button class="btn btn-outline-success">Read More</button>
</div>
        </div>
    </div>
</section>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/footer.php";?>
</body>
</html>

