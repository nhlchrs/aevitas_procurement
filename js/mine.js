
$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});

});

	// Login Button
	$('#loginBtn').on('click', function(){
		var email = $('#userEmail').val();
		var username =$('#userEmail').attr('rel');
		if(email == ''){
			$('#error').text("Seriously? Without email");
			$('#error').attr('hidden', false);
			return;
		}
		var password = $('#userPassword').val();
		if(password ==''){
			$('#error').text("Seriously? Without password");
			$('#error').attr('hidden', false);
			return;
		}
	})

	// Sign button

	$('#signUpBtn').on('click',function(){
		var email = $('#userEmail').val();
		if(email == ''){
			$('#error').text("Seriously? Without email");
			$('#error').attr('hidden', false);
			return;
		}
		var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
		if (!testEmail.test(email)){
    		$('#error').text("Seriously? With Wrong email Address");
			$('#error').attr('hidden', false);
			return;
    	}
		var username = $('#userName').val();
		if(username == ''){
			$('#error').text("Seriously? Without username");
			$('#error').attr('hidden', false);
			return;
		}
		else if(username.length<4){
			$('#error').text("Username Length should be atleast 6 Characters");
			$('#error').attr('hidden', false);
			return;
		}
		var password = $('#userPassword').val();
		if(password ==''){
			$('#error').text("Seriously? Without password");
			$('#error').attr('hidden', false);
			return;
		}
		else if(password.length<3){
			$('#error').text("Password length should be atleast 8 Characters");
			$('#error').attr('hidden', false);
			return;
		}
		var datastring = 'email='+ email + ' &username='+username+ '&password='+password;
		$.ajax({
			type:'POST',
			url:'ajax/ajaxRegister',
			data:datastring,
			success:function(data){
				if(data=='success'){
					$('#success').text("Successfully Registered With Aevitas");
					$('#success').attr('hidden', false);
					$('#error').hide();
					setTimeout(function(){ window.location.replace('/login'); }, 2000);
				}
				else{
					$('#error').text("Unexpected Error Occurred");
					$('#error').attr('hidden', false);
				}
			}
		})
	});

	//Contact Us form..

	$('#contact_us').on('click', function(){
		var fullname = $('#fullname').val();
		var phoneNo = $('#userPhone').val();
		var email = $('#form_email').val();
		var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
		if (!testEmail.test(email)){
    		$('#error').text("Seriously? With Wrong email Address");
			$('#error').attr('hidden', false);
			return;
    	}
		var requestType = $('#requestType option:selected').val();
		var message = $('#form_message').val();
		if(fullname == '' || fullname.length<5)
		{
			$('#error').text("You have No name or Your Name is too short?");
			$('#error').attr('hidden', false);
			return;
		}
		if(phoneNo == '' || phoneNo.length!=10)
		{
			$('#error').text("What you have no Number Or invalid  Number??");
			$('#error').attr('hidden', false);
			return;
		}
		if(email == '')
		{
			$('#error').text("You have No Email, How we will Contact then???");
			$('#error').attr('hidden', false);
			return;
		}
		if(message == '')
		{
			$('#error').text("Specify your need...!!");
			$('#error').attr('hidden', false);
			return;
		}
		var datastring = 'fullname='+fullname+'&phoneNo='+phoneNo+'&email='+email+'&requestType='+requestType+'&message='+message;
		alert(datastring);
		$.ajax({
			type:'POST',
			url:'ajax/ajaxContactUs',
			data:datastring,
			success:function(data){
				if(data){
					alert("Successfully Send To admin");
					location.reload();
				}
			}

		})
});

$('#insertProducts').click(function(){
	var productName= $('#productName').val();
	var categoryImages = $('#categoryImages').val();
	if(productName=='')
	{
		alert("Without Product Name?");
		return;
	}
	if(categoryImages==''){
		alert("Please Select Images");
		return;
	}
	var extension = $('#categoryImages').val().split('.').pop().toLowerCase();
		if($.inArray(extension,['gif','png','jpg','jpeg'])==-1){
			alert("Invalid Images");
			$('#categoryImages').val('');
			return false;
		}
});

$('.categoryDetails').click(function(){
	var adminOrUser = $('#adminOrUser').attr('rel');
	var details = $(this).attr('id');
	if(adminOrUser=='Y'){
		// location.href='/admin/addSubCategory?category='+details;
		location.href='/subcategory+?category='+details;
		var datastring ='details='+details;
	}
	else{
		location.href='/subcategory'+"?category="+details;
	}	
})

$('.subcategoryDetails').click(function(){
	var adminOrUser = $('#adminOrUser').attr('rel');
	var details = $(this).attr('id');
	if(adminOrUser=='Y'){
		location.href='/admin/addFinalProducts?subcategory='+details;
		var datastring ='details='+details;
	}
	else{
		location.href='/product'+"?subcategory="+details;
	}	
})

$('#insertSubProducts').click(function(){
	var subcategoryName= $('#subcategoryName').val();
	var subcategoryImages = $('#subcategoryImages').val();
	if(subcategoryName=='')
	{
		alert("Without Product Name?");
		return;
	}
	if(subcategoryImages==''){
		alert("Please Select Images");
		return;
	}
	var extension = $('#subcategoryImages').val().split('.').pop().toLowerCase();
		if($.inArray(extension,['gif','png','jpg','jpeg'])==-1){
			alert("Invalid Images");
			$('#subcategoryImages').val('');
			return false;
		}
});
$('#insertFinalProducts').click(function(){
	var finalcategoryName= $('#finalcategoryName').val();
	var finalcategoryImages = $('#finalcategoryImages').val();
	if(finalcategoryName=='')
	{
		alert("Without Product Name?");
		return;
	}
	if(finalcategoryImages==''){
		alert("Please Select Images");
		return;
	}
	var extension = $('#finalcategoryImages').val().split('.').pop().toLowerCase();
		if($.inArray(extension,['gif','png','jpg','jpeg'])==-1){
			alert("Invalid Images");
			$('#finalcategoryImages').val('');
			return false;
		}
});
$('#searchUser').on('click', function(){
	var userValue = $('#userValue').val();
	if(userValue.length<=5){
		alert("Username should be greater than 5");
		$('#search-result').hide();
		return;
	}
	var datastring='userValue='+userValue;
	$.ajax({
		type:'POST',
		url:'../admin/ajax/ajaxSearchUser',
		data:datastring,
		success:function(data){
			if(data){
				$('#search-result').show().html(data);
			}
			else{
				$('#search-result').hide();
			}
		}
	})
})
$('#userValue').on('keyup', function(){
	var userValue = $('#userValue').val();
	if(userValue.length<=2){
		$('.author-search-result').hide();
		return;
	}
	var datastring='userValue='+userValue;
	$.ajax({
		type:'POST',
		url:'../admin/ajax/ajaxSearchUser',
		data:datastring,
		success:function(data){
			if(data){
				$('.author-search-result').show();
				$('#author-search-result').html(data);
			}
		}
	})
})
$('#search-term').focus(function()
{
    $(this).animate({ width: '+=50' }, 'slow');
}).blur(function()
{
    $(this).animate({ width: '-=50' }, 'slow');
});

$(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll >= 10) {
        $("#sticky-navbar").addClass("sticky-navbar");
    }
    else{
    	$("#sticky-navbar").removeClass("sticky-navbar");
    }
});

$('#search-term').on('keyup',function(){
	var searchTerm = $('#search-term').val();
	if(searchTerm.length<3){
		$('.suggestion-search-result').hide();
		return;
	}
	var datastring="searchTerm="+searchTerm;
	$.ajax({
		type:'POST',
		url:'/ajax/ajaxSearchWebsite',
		data:datastring,
		success:function(data){
			if(data){
				$('.suggestion-search-result').show().html(data);
			}
		}
	})
})
$('.suggestion-search-result').hide();