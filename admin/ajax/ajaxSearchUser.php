<?php 
include $_SERVER['DOCUMENT_ROOT']."/core/db.php"; 
include $_SERVER['DOCUMENT_ROOT']."/helper/searchHelper.php";
include $_SERVER['DOCUMENT_ROOT']."/include/session.php";
$search = new searchHelper();
if(isset($_POST['userValue'])){
$searchName= $_POST['userValue'];
$getUsersDetails = $search->searchUser($searchName);
if($getUsersDetails){
	echo '<div class="row">';
	foreach ($getUsersDetails as $key) {
		echo '<div class="col-12 space-10" >';
		echo '<ul id="author-search-result">';
		echo $key->email."  <b>and fullname is:</b>  ".$key->full_name;
		 echo '<div class="card card-simple space-10">';
            echo '<h4 class="varela">';
            echo '<a href="/profile.php?id='.$key->id.'" class="inverted" target="_blank">';
            echo (!empty($key->full_name)) ? $key->full_name : $key->username;
            echo ' <small>'.$key->username.'</small>';
            echo '</a></h4>';
            echo '<div class="form-inline margin-10">';
            echo '<div class="form-group">';
            echo '<input type="text" id="author_rate" class="form-control" placeholder="Rate in paise/word" disabled/> ';
            echo '</div>';
            echo ' <a id="'.$key->id.'-'.$key->email.'" rel="add" class="btn btn-success add_remove_author"><span class="glyphicon glyphicon-plus"></span> Add</a>';
            echo '</div>';
            echo '<p><small>Rate is only required for author role.</small></p>';
            echo '</div>';
		echo '</div>';
		echo '</div>';
	}
		echo '</div>';

}
else{
	echo "<div class='author-search-result'><b>No user Found with this Name/Email</b></div>";
}
}
?>