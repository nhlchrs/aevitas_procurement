<?php 
include $_SERVER['DOCUMENT_ROOT']."/core/db.php";
include $_SERVER['DOCUMENT_ROOT']."/include/session.php";
include $_SERVER['DOCUMENT_ROOT']."/helper/productHelper.php";
if(!isset($_SESSION['id'])){
	header('location:index');
}
$error_code=null;
$error_desc=null;
if(isset($_GET['subcategory'])){
if(isset($_POST['submit']) && $_POST['finalcategoryName']){
  $finalProductsName = $_POST['finalcategoryName'];
  $subcategory_id = $_GET['subcategory'];
  $file =addslashes(file_get_contents($_FILES['finalcategoryImages']['tmp_name']));
  $query = "INSERT INTO final_products(finalproductsName, finalproductsImages, subcategory_id) VALUES ('$finalProductsName' , '$file' ,'$subcategory_id' )";
    if(mysqli_query($connect, $query)){
    $error_desc="Products Images Inserted Succefully";
    $error_code='1';
    header('location:/admin/addFinalProducts');
  }
  else{
    $error_desc="Oops! Some error Occured";
    $error_code='2'; 
  }
}
}
else{
  header('location:/subcategory');
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
  <link rel="shortcut icon" type="text/css" href="../images/apspl.jpg">
  <link rel="stylesheet" type="text/css" href="../css/fonts/font-awesome.css">
    <title>Add FInal products | Aevitas Procurement Service </title>
</head>
<body>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/navbar.first.php";?>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/navbar.php"; ?>
<div class="container margin-t-10">
<table class="table table-bordered container">
  <tr>
    <td>
  <h1 class="text-center space-10 heading-color">Add Final Products</h1>
</td></tr>
<tr>
  <td>
	<form method="POST" enctype="multipart/form-data">
	<div class="form-group">
    <label for="">Products Name:</label>
    <input type="text" class="form-control" id="finalcategoryName" name="finalcategoryName" placeholder="Enter Product Name">
  </div>
</td>
</tr>
<tr>
<td>
  <div class="form-group">
    <label for="">Products Images:</label>
    <input type="file" class="form-control-file"  name="finalcategoryImages" id="finalcategoryImages">
  </div>
</td>
</tr>
<tr><td>
  <input type="submit" class="btn btn-success" name="submit" id="insertFinalProducts" value="Insert Products">
  </form>
  <br><br>
  <?php if($error_code=='1'){
  echo '<div class="success-msg">'.$error_desc.'</div>';
  }
  if($error_code=='2'){
    echo '<div class="error-msg">'.$error_desc.'</div>';
  }?>
</td>
</tr>
</table>
</div>
<br><br>
<?php 
include $_SERVER['DOCUMENT_ROOT'].'/include/footer.php';?>
</body>
</html>