<?php 
include $_SERVER['DOCUMENT_ROOT']."/core/db.php";
include $_SERVER['DOCUMENT_ROOT']."/include/session.php";
include $_SERVER['DOCUMENT_ROOT']."/helper/searchHelper.php";
if(!isset($_SESSION['id'])){
	header('location:index');
}
if(isset($_SESSION['is_admin']) && $_SESSION['is_admin']=='Y'){
	$searchHelper = new searchHelper();
	$search='nihal';
	$getUsersDetails = $searchHelper->searchUser($search);
	foreach ($getUsersDetails as $key) {	}
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
	<link rel="shortcut icon" type="text/css" href="../images/apspl.jpg">
	<link rel="stylesheet" type="text/css" href="../css/fonts/font-awesome.css">
    <title>Admin Contact | Aevitas Procurement Service </title>
</head>

<body>
	<?php
	include $_SERVER['DOCUMENT_ROOT']."/include/navbar.first.php";
	include $_SERVER['DOCUMENT_ROOT']."/include/navbar.php"; 
	?>
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-sm-12 col-md-12">
				<div class="heading-color text-center space-20">
				<h1>Assign User Role</h1><hr>
				</div>
			</div>
		</div>
						<div class="form-inline form-group">
						<input type="text" name="" placeholder="Type any username or email to search User" class="form-control" id="userValue"><button class="btn btn-info pull-left" value=""  id="searchUser"><span class="fa fa-search"></span></button>
						</div>
			<div class="author-search-result" id="author-search-result"></div>
	</div><br><br><br><br><br>
	<?php include $_SERVER['DOCUMENT_ROOT']."/include/footer.php"; ?>
</body>
</html>