<?php 
include $_SERVER['DOCUMENT_ROOT']."/core/db.php";
include $_SERVER['DOCUMENT_ROOT']."/include/session.php";
include $_SERVER['DOCUMENT_ROOT']."/helper/productHelper.php";
if(!isset($_SESSION['id'])){
	header('location:index');
}
$error_code=null;
$error_desc=null;
if(isset($_GET['category'])){
if(isset($_POST['submit']) && $_POST['subcategoryName']){
  $subcategoryName = $_POST['subcategoryName'];
  $category_id = $_GET['category'];
  $file =addslashes(file_get_contents($_FILES['subcategoryImages']['tmp_name']));
  $query = "INSERT INTO subcategory(subcategoryName, subcategoryImages, category_id) VALUES ('$subcategoryName' , '$file' ,'$category_id' )";
  if(mysqli_query($connect, $query)){
    $error_desc="Sub Category Inserted Succefully";
    $error_code='1';
    header('location:/admin/addSubCategory');
  }
  else{
    $error_desc="Oops! Some error Occured";
    $error_code='2'; 
  }
}
}
else{
  header('location:/products');
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
  <link rel="shortcut icon" type="text/css" href="../images/apspl.jpg">
  <link rel="stylesheet" type="text/css" href="../css/fonts/font-awesome.css">
    <title>Add Sub-Category | Aevitas Procurement Service </title>
</head>
<body>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/navbar.first.php";?>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/navbar.php"; ?>
<div class="container margin-t-10">
<table class="table table-bordered container">
  <tr>
    <td>
  <h1 class="text-center space-10 heading-color">Add Sub-Category</h1>
</td></tr>
<tr>
  <td>
	<form method="POST" enctype="multipart/form-data">
	<div class="form-group">
    <label for="">Sub Category Name:</label>
    <input type="text" class="form-control" id="subcategoryName" name="subcategoryName" placeholder="Enter Product Name">
  </div>
</td>
</tr>
<tr>
<td>
  <div class="form-group">
    <label for="">Sub-Category Images</label>
    <input type="file" class="form-control-file"  name="subcategoryImages" id="subcategoryImages">
  </div>
</td>
</tr>
<tr><td>
  <input type="submit" class="btn btn-success" name="submit" id="insertSubProducts" value="Insert Sub-Category">
  </form>
  <br><br>
  <?php if($error_code=='1'){
  echo '<div class="success-msg">'.$error_desc.'</div>';
  }
  if($error_code=='2'){
    echo '<div class="error-msg">'.$error_desc.'</div>';
  }?>
</td>
</tr>
</table>
</div>
<br><br>
<?php 
include $_SERVER['DOCUMENT_ROOT'].'/include/footer.php';?>
</body>
</html>