<?php 
include $_SERVER['DOCUMENT_ROOT']."/core/db.php";
include $_SERVER['DOCUMENT_ROOT']."/include/session.php";
include $_SERVER['DOCUMENT_ROOT']."/helper/productHelper.php";
if(!isset($_SESSION['id'])){
	header('location:/index');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css">
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.min.js"></script>
	<link rel="shortcut icon" type="text/css" href="../images/apspl.jpg">
	<link rel="stylesheet" type="text/css" href="../css/fonts/font-awesome.css">
    <title>Admin | Aevitas Procurement Service </title>

    <style type="text/css">
    	div>a{
    		text-decoration: none;
    	}
    </style>
</head>

<body>
	<?php
	include $_SERVER['DOCUMENT_ROOT']."/include/navbar.first.php";
include $_SERVER['DOCUMENT_ROOT']."/include/navbar.php"; 
	?>
<br>
<?php if(isset($_SESSION['is_admin']) && $_SESSION['is_admin']=='Y'){?>
<div class="container">
	<div class="row">
		<div class="col-md-12 col-sm-12 col-sm-12">
			<h1 class="text-center space-10 heading-color">Admin </h1>
			<hr>
		</div>
	</div>
	<div class="row space-10">
		<div class="col-md-4 col-lg-4 col-sm-12 space-10">
			<div class="card card-light">
				<div class="space-20 text-center "><a style="text-decoration: none;" href="/admin/addproducts" class="space-20"><span class="fa fa-plus"></span> Add Products</a></div>
			</div>
		</div>
		<div class="col-md-4 col-lg-4 col-sm-12 space-10">
			<div class="card card-light">
				<div class="space-20 text-center "><a style="text-decoration: none;" href="/admin/userRole" class="space-20"><span class="fa fa-plus"></span> User Access</a></div>
			</div>
		</div>
		<div class="col-md-4 col-lg-4 col-sm-12 space-10">
			<div class="card card-light">
				<div class="space-20 text-center"><a style="text-decoration: none;" href="/admin/addSubCategory" class="space-20"><span class="fa fa-plus"></span> Add Sub Category</a></div>
			</div>
		</div>
	</div>
	<div class="row space-10">
		<div class="col-md-4 col-lg-4 col-sm-12 space-10">
			<div class="card card-light">
				<div class="space-20 text-center "><a style="text-decoration: none;" href="/admin/addFinalProducts" class="space-20"><span class="fa fa-plus"></span> Add Final Products</a></div>
			</div>
		</div>
		<div class="col-md-4 col-lg-4 col-sm-12 space-10">
			<div class="card card-light">
				<div class="space-20 text-center "><a style="text-decoration: none;" href="/admin/userRole" class="space-20"><span class="fa fa-plus"></span> User Access</a></div>
			</div>
		</div>
		<div class="col-md-4 col-lg-4 col-sm-12 space-10">
			<div class="card card-light">
				<div class="space-20 text-center"><a style="text-decoration: none;" href="/admin/addSubCategory" class="space-20"><span class="fa fa-plus"></span> Add Sub Category</a></div>
			</div>
		</div>
	</div>
</div>
<?php }else{
	echo "You are not a Admin";	}
	?><br><br>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/footer.php"; ?></body>
</html>