<?php 
class productHelper{
	public static function getProductsDetails(){
		try{
			$db=getDB();
			$stmt=$db->prepare("SELECT * FROM products");
			$productsDetails=array();
			if($stmt->execute()){
				while($row=$stmt->fetch(PDO::FETCH_OBJ)){
					$productsDetails[]=$row;
				}
			}
			$db=null;
			return $productsDetails;
		}
		catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
	}


	public static function addProductsDetails($productname, $productImage)
	{
		try{
			$db=getDB();
			$stmt = $db->prepare('INSERT INTO products(category, categoryImages) VALUES (:category, :categoryImages)');
			$stmt->bindParam('category', $productname , PDO::PARAM_STR);
			$stmt->bindParam('categoryImages', $productImage, PDO::PARAM_LOB);
			$stmt->execute();
			$db=null;
			return 'success';
		}
		catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
	}

	public static function getsubCategoryDetails($specificId)
	{
		try{
			$db=getDB();
			$stmt = $db->prepare('SELECT S.id, S.subcategoryName, S.subcategoryImages from subcategory S, products P where S.category_id=P.id and S.category_id=:specificId');
			$stmt->bindParam('specificId', $specificId, PDO::PARAM_INT);
			$subcategoryDetails=array();
			if($stmt->execute()){
				while ($row=$stmt->fetch(PDO::FETCH_OBJ)) {
					$subcategoryDetails[]=$row;
				}
			}
			$db=null;
			return $subcategoryDetails;
		}
		catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
	}

	public static function getFinalProductDetails($specificId)
	{
		try{
			$db=getDB();
			$stmt = $db->prepare('SELECT F.final_id, F.finalproductsName, F.finalproductsImages from subcategory S, final_products F where F.subcategory_id=S.id and F.subcategory_id=:specificId');
			$stmt->bindParam('specificId', $specificId, PDO::PARAM_INT);
			$finalProductsDetails=array();
			if($stmt->execute()){
				while ($row=$stmt->fetch(PDO::FETCH_OBJ)) {
					$finalProductsDetails[]=$row;
				}
			}
			$db=null;
			return $finalProductsDetails;
		}
		catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
	}
}


?>