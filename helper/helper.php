<?php
class Helper{
public static function insertUsers($fullname, $username, $email, $password, $validation_code){
  try{
    $db=getDB();
    $validation_code=  md5(microtime());
    $encrypt_password = md5($password);
    $username_exists = $db->prepare('SELECT  id from users where username=:username');
    $username_exists->bindParam('username', $username , PDO::PARAM_STR);
    $username_exists->execute();
    if($username_exists->rowCount()>0){
      return 'username_exists';
    }
    $email_exists = $db->prepare('SELECT  id from users where email=:email');
    $email_exists->bindParam('email', $email , PDO::PARAM_STR);
    $email_exists->execute();
    if($email_exists->rowCount()>0){
      return 'email_exists';
    }
    $stmt = $db->prepare("INSERT into users (full_name,username,email,password,validation_code, active) VALUES (:fullname,:username,:email, :password, :validation_code, 0)");
    $stmt->bindParam('fullname', $fullname , PDO::PARAM_STR);
    $stmt->bindParam('username', $username , PDO::PARAM_STR);
    $stmt->bindParam('email', $email , PDO::PARAM_STR);
    $stmt->bindParam('password', $encrypt_password , PDO::PARAM_STR);
    $stmt->bindParam('validation_code', $validation_code , PDO::PARAM_STR);
    if($stmt->execute()){
    return 'success';
  }
  else{
    return 'error';
  }
  }
  catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}


public static function logging_in($username, $password){
  try{
    $db=getDB();
    $secure_password = md5($password);
    $stmt = $db->prepare("SELECT * FROM users where (email=:username or username=:username)");
    $stmt->bindParam('username', $username , PDO::PARAM_STR);
    $stmt->execute();
    if($stmt->rowCount()>0){
      $data = $stmt->fetch(PDO::FETCH_OBJ);
      if($data){
      if($secure_password == $data->password){
        $_SESSION['id'] = $data->id;
        $_SESSION['username']= $data->username;
        $_SESSION['email'] = $data->email;
        $_SESSION['fullname']=$data->full_name;
        $_SESSION['is_admin']=$data->is_admin;
        if($_SESSION['id']){
          header('location:index.php');
        }
        else{
          header('location:login.php');
        }
      } 
      else{
        return 'mismatch';
      } 
    }
  }
  else{
    return 'error';
  }
  }
  catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
}
}
 ?>
