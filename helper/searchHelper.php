<?php
class searchHelper{
	public static function searchUser($search){
		try{
			$search = '%'.$search.'%';
			$db= getDB();
			$stmt=$db->prepare('SELECT * from users where full_name like :search or username like :search or email like :search LIMIT 5');
			$stmt->bindParam("search" , $search ,PDO::PARAM_STR);
			$userDetails = array();
			if($stmt->execute()){
				while ($row=$stmt->fetch(PDO::FETCH_OBJ)) {
					$userDetails[]=$row;
				}
			}
			$db=null;
			return $userDetails;

		}
		catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
	}
	public static function searchWebsite($search){
		try{
			$search = '%'.$search.'%';
			$db= getDB();
			$stmt=$db->prepare('SELECT S.subcategoryName, P.category from products P, subcategory S where S.subcategoryName like :search or  P.category like :search LIMIT 5');
			$stmt->bindParam("search" , $search ,PDO::PARAM_STR);
			$webDetails = array();
			if($stmt->execute()){
				while ($row=$stmt->fetch(PDO::FETCH_OBJ)) {
					$webDetails[]=$row;
				}
			}
			$db=null;
			return $webDetails;

		}
		catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}';
    }
	}
}
?>