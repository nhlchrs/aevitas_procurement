<?php 
include "./core/db.php";
include $_SERVER['DOCUMENT_ROOT']."/include/session.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <link rel="shortcut icon" type="text/css" href="/images/apspl.jpg">
    <link rel="stylesheet" type="text/css" href="/css/fonts/font-awesome.css">
    <title>About us | Aevitas Procurement Service </title>
</head>

<body>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/navbar.first.php";?>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/navbar.php"; ?>

<div>
	<div class="row">
		<div class="col-md-12 col-lg-12 col-xs-12 ">
			<img src="./images/aboutvendor.jpg" class="aa" style="opacity:1;">
			<h1 class="imgsetting">About Us </h1>
			<span class="imgsettings"> <i class="fa fa-home" aria-hidden="true"> &gt; About</i></span>
		</div>
	</div>
	<div class="row" style="margin-top: 40px;">
		<div class="col-md-6 col-lg-6 text-center">
			<img src="./images/about2.jpg" class="about-image" >
		</div>
		<div class="col-md-6 col-lg-6 text-center">
		<p class="about-para">We are an Indian Based E-Commerce Company with some heightened goals to serve our Client with our full energy and make satisfy with our services which we are providing. We intensively inclined toward B2B (Business to Business) marketplace procurement of Industrial as well as Institutional Organization supplies like, safety, Electricals, Lighting, Computer Accessories,CCTV, UPS, Inverter, Networking Equipments, Medical Equipments, Office Stationary & Supplies, Home Appliances, Power Tools and many more other utilities. <br><br> Our main aim is to solve the complicated problems of Corporate Sourcing and make the market more easy and fair for the Customers. By this we want to make a better platform for Seller to sale the product and Buyer to purchase the product.
</p>
		</div>
	</div>
	<p class="col-md-12  col-lg-12 col-sm-12 about-para"><br>
We have a strong belief that we take pride in delivering World-Class Services to our Clients with satisfying goods of optimum quality and services so that we can maintain a healthy relationship with our Customers. To attain such position in market our Team Members experience, ability to work under pressure and dedication helps us to achieve all our business objectives in an efficient and timely vogue. Our belief is to serve every day through an exceptional service to our Clients for experience a good and healthy relation between us.</p>
</div>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/footer.php"; ?>
</body>
</html>