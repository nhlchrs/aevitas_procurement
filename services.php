<?php 
include $_SERVER['DOCUMENT_ROOT']."/core/db.php";
include $_SERVER['DOCUMENT_ROOT']."/include/session.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
  <link rel="shortcut icon" type="text/css" href="./images/apspl.jpg">
  <link rel="stylesheet" type="text/css" href="/css/fonts/font-awesome.css">
    <title>Services | Aevitas Procurement Service </title>
</head>
<style type="text/css">
  
</style>
<body>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/navbar.first.php";
include $_SERVER['DOCUMENT_ROOT']."/include/navbar.php"; ?>
<div style="background: lightgrey">
<div class="row">
		<div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
			<img src="./images/aboutvendor.jpg" class="aa" style="opacity:1;">
			<h1 class="imgsetting">Services </h1>
			<span class="imgsettings"> <i class="fa fa-home" aria-hidden="true"> &gt; Services</i></span>
		</div>
	</div>

<div class="row" style="margin-top: 20px;">
		<div class="col-md-6 col-lg-4 col-sm-12">
			<div class="service-align">
  <img src="./images/Software-Installation-in-Desktops-and-Laptops.png" class="image" style="width:100%">
  <h3 class="text-center text-style">Software Installation</h3>
  <div class="middle-services">
    <div class="text">Let's Talk</div>
  </div>
	</div>
</div>
	<div class="col-md-6 col-lg-4 col-sm-12">
		<div class="service-align">
  <img src="./images/Window-AC-Repairing.png" class="image" style="width:100%">
  <h3 class="text-center text-style">Window AC Repairing</h3>
  <div class="middle-services">
    <div class="text">Let's Talk</div>
  </div>
</div>
	</div>
	<div class="col-md-6 col-lg-4 col-sm-12">
		<div class="service-align">
  <img src="./images/Switch-Socket-Installation.png" class="image" style="width:100%">
  <h3 class="text-center text-style">Switch Socket Installation</h3>
  <div class="middle-services">
    <div class="text">Let's Talk</div>
  </div>
	</div>
</div>
</div>
<div class="row" style="margin-top: 20px;">
		<div class="col-md-6 col-lg-4 col-sm-12">
			<div class="service-align">
  <img src="./images/split-ac.png" class="image" style="width:100%">
  <h3 class="text-center text-style">Split AC</h3>
  <div class="middle-services">
    <div class="text">Let's Talk</div>
  </div>
	</div>
</div>
	<div class="col-md-6 col-lg-4 col-sm-12">
		<div class="service-align">
  <img src="./images/Split-AC-Repairing.png" class="image" style="width:100%">
  <h3 class="text-center text-style">Split AC Repairing</h3>
  <div class="middle-services">
    <div class="text">Let's Talk</div>
  </div>
</div>
	</div>
	<div class="col-md-6 col-lg-4 col-sm-12">
		<div class="service-align">
  <img src="./images/Split-AC-Installation.png" class="image" style="width:100%">
  <h3 class="text-center text-style">Window-AC-Repairing</h3>
  <div class="middle-services">
    <div class="text">Let's Talk</div>
  </div>
	</div>
</div>
</div>
<div class="row" style="margin-top: 20px;">
		<div class="col-md-6 col-lg-4 col-sm-12">
			<div class="service-align">
  <img src="./images/Projector-Repairing.png" class="image" style="width:100%">
  <h3 class="text-center text-style">Projector Repairing</h3>
  <div class="middle-services">
    <div class="text">Let's Talk</div>
  </div>
	</div>
</div>
	<div class="col-md-6 col-lg-4 col-sm-12">
		<div class="service-align">
  <img src="./images/Projector-Installation.png" class="image" style="width:100%">
  <h3 class="text-center text-style">Projector Installation</h3>
  <div class="middle-services">
    <div class="text">Let's Talk</div>
  </div>
</div>
	</div>
	<div class="col-md-6 col-lg-4 col-sm-12">
		<div class="service-align">
  <img src="./images/Ceiling-Fan-Installation.png" class="image" style="width:100%">
  <h3 class="text-center text-style">Ceiling Fan Installations</h3>
  <div class="middle-services">
    <div class="text">Let's Talk</div>
  </div>
	</div>
</div>
</div>
<div class="row" style="margin-top: 20px;">
		<div class="col-md-6 col-lg-4 col-sm-12">
			<div class="service-align">
  <img src="./images/CCTV-Camera-Installation.png" class="image" style="width:100%">
  <h3 class="text-center text-style">CCTV Camera Installation</h3>
  <div class="middle-services">
    <div class="text">Let's Talk</div>
  </div>
	</div>
</div>
	<div class="col-md-6 col-lg-4 col-sm-12">
		<div class="service-align">
  <img src="./images/Domestic-Purifier-Installation.png" class="image" style="width:100%">
  <h3 class="text-center text-style"> Purifier Installation</h3>
  <div class="middle-services">
    <div class="text">Let's Talk</div>
  </div>
</div>
	</div>
	<div class="col-md-6 col-lg-4 col-sm-12">
		<div class="service-align">
  <img src="./images/Domestic-RO-Installation.png" class="image" style="width:100%">
  <h3 class="text-center text-style">Domestic RO Installation</h3>
  <div class="middle-services">
    <div class="text">Let's Talk</div>
  </div>
	</div>
</div>
</div>
<div style="margin-top: 40px;"></div>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/footer.php"; ?>
</div>
</body>
</html>