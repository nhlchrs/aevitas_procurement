<?php 
include $_SERVER['DOCUMENT_ROOT']."/core/db.php";
include $_SERVER['DOCUMENT_ROOT']."/include/session.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="shortcut icon" type="text/css" href="./images/apspl.jpg">
    <link rel="stylesheet" type="text/css" href="/css/fonts/font-awesome.css">
    <title>Profile | Aevitas Procurement Service </title>
</head>
<body>
	<?php
	include $_SERVER['DOCUMENT_ROOT']."/include/navbar.first.php";
	include $_SERVER['DOCUMENT_ROOT']."/include/navbar.php"; ?>
	<br>



<?php include $_SERVER['DOCUMENT_ROOT']."/include/footer.php";?>
</body>
</html>