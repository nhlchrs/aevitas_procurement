<div  id="sticky-navbar">
<nav class="navbar navbar-expand-lg navbar-light bg-light" style="width: 100%" id="myHeader">

  <a class="navbar-brand" href="index.php"><img class='img-logo' src=<?php echo isset($image)? './images/apspl.jpg' :'../images/apspl.jpg';?>></a>
    <div class="pull-left search-box">
      <input class="form-control" placeholder="Search" name="search-term" id="search-term" type="text">
      <div id="suggestion-search-result" class="suggestion-search-result"></div>
    </div>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="nav-item nav-link small-show">
    <?php
    if(isset($_SESSION['id'])){
          if(isset($_SESSION['is_admin']) && $_SESSION['is_admin']=='Y'){
           echo ' <a class="nav-item nav-link large-show" href="/admin/" ><span class="nav-link nav-item ml-auto text-center fa fa-cog"></span></a>'; 
         }
           echo ' <a class="nav-item nav-link large-show" href="/logout"><span class="nav-link nav-item text-center fa fa-sign-out"></span></a>'; 
      }

     ?>
  </div>
  <div class="collapse navbar-collapse screen-size" id="navbarNavAltMarkup">
    <div class="navbar-nav text-center ml-auto text-center" style="" >
      <a class="nav-item nav-link" href="/index">Home <span class="sr-only">(current)</span></a>
      <a class="nav-item nav-link" href="/about">About</a>
      <a class="nav-item nav-link" href="/catalogue">Catalogue</a>
      <a class="nav-item nav-link" href="/services">Services </a>
      <a class="nav-item nav-link" href="/contact_us">Contact</a>
      <?php 
      if(isset($_SESSION['id'])){
        echo '<a class="nav-link nav-item" href="/profile" href="/profile" target="_blank"><b>'.ucfirst($_SESSION['username']).'</b></a>';
        if(isset($_SESSION['is_admin']) && $_SESSION['is_admin']=='Y'){
           echo ' <a class="nav-item nav-link large-show" href="/admin/" ><span class="nav-link nav-item ml-auto text-center fa fa-cog"></span></a>'; 
         }
           echo ' <a class="nav-item nav-link large-show" href="/logout"><span class="nav-link nav-item text-center fa fa-sign-out"></span></a>'; 
      }
      else{
        echo '<a class="nav-item nav-link main-nav" href="/login.php">Sign In</a>';
      }
      ?>
    </div>
  </div>
</nav>
</div>