<div class="sticky-footer">
<section class="footer " style="background: #222">
    <div class="row text-center">
        <div class="col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto " style="">
<div class="d-block m-auto">
<h4 style="margin-top: 20px;margin-bottom: 20px; ">About Aevitas</h4>
<p>We are an Indian Based E-Commerce Company with some heightened goals to serve our Client with our
full energy and make satisfy with our services which we are providing.</p>

</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto " style="">
<div class=" d-block m-auto">
<h4 style="margin-top: 20px;margin-bottom: 20px;">Contact Information</h4>
<p><i class="fa fa-home" style="color: white;"></i>  3rd floor, 2895, Gate No.4, Chuna Mandi, Paharganj, New Delhi- 110055</p>
<p><i class="fa fa-home" style="color: white;"></i>  Ground Floor, H No. 145, Block 3A, Bishnipur, near Shia Masjid, Ballia Uttar Pradesh-277001</p>
</div>
</div>
<div class="col-lg-4 col-md-4 col-sm-4 col-10 d-block m-auto " style="">
<div class=" d-block m-auto">
<h4 style="">Stay Connected</h4>
<a href="#"><i class="fa fa-facebook-square fa-icon" style="font-size:48px;color: white;"></i></a>&nbsp;
<a href="#"><i class="fa fa-linkedin-square fa-icon" style="font-size:48px;color: white;"></i></a>&nbsp;
<a href="#"><i class="fa fa-twitter-square fa-icon" style="font-size:48px;color: white;"></i></a>&nbsp;
<a href="#"><i class="fa fa-instagram fa-icon" style="font-size:48px; color: white;"></i></a>
<div style="margin-top: 10px;">
<i class="fa fa-phone" style="color: white;margin-bottom: 10px;"></i>  +91-7897438810 <br>
<i class="fa fa-envelope" style="color: white;"></i>  info@aevitasproc.com
</div>

</div>
    </div>
</div>

</section>
<div style="background: #1B1B1B;" class="text-center footer" >
    <h6 style="color:white; padding: 20px 20px 20px 20px;">Copyright 2019 - Aevitas Procurement Service Private Limited</h6>
</div>
</div>
<!-- <div class="mapouter"><div class="gmap_canvas"><iframe width="600" height="500" id="gmap_canvas" src="https://maps.google.com/maps?ll=28.5449756,77.1904397&q=Indian Institute of Technology Delhi&t=&z=14&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><a href="https://techwithlove.com/"></a></div><style>.mapouter{position:relative;text-align:right;height:500px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:600px;}</style></div> -->
<script src="../js/mine.js"></script></html>