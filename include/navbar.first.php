 <div class="row">
    <div class="col-md-6 col-sm-12 col-lg-6 text-center">
      <a class="navbar-brand ml-3 upper" href="mailto:Info@aevitasproc.com">
    <span class=""> <i class="fa fa-envelope" aria-hidden="true"></i> &nbsp;<i style="font-size: 18px">Info@aevitasproc.com</i></span>
</a>
   <a class="navbar-brand ml-3 upper" href="tel:+91-7897438810"> <span class="" style="font-size: 18px"> <i class="fa fa-mobile" aria-hidden="true"></i> &nbsp;<i>+91-7897438810</i></span>
     </a>
    </div>
    <div class="col-md-6 col-sm-12 col-lg-6 text-center">
        <a href="instagram" class="navbar-brand ml-3 upper"><i class="fa fa-instagram" aria-hidden="true"></i></a>
        <a href="twitter" class="navbar-brand ml-3 upper"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        <a href="facebook" class="navbar-brand ml-3 upper"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="linkedin" class="navbar-brand ml-3 upper"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
        
    </div>
</div>