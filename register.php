
<?php 
include $_SERVER['DOCUMENT_ROOT'].'/core/db.php';
include $_SERVER['DOCUMENT_ROOT'].'/helper/helper.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
	<link rel="shortcut icon" type="text/css" href="./images/apspl.jpg">
	<link rel="stylesheet" type="text/css" href="/css/fonts/font-awesome.css">
    <title>Register | Aevitas Procurement Service </title>

<head>
</head><body>
	<?php
	include $_SERVER['DOCUMENT_ROOT']."/include/navbar.first.php";
	include $_SERVER['DOCUMENT_ROOT']."/include/navbar.php"; 
	if(isset($_POST['full_name'])){
		$fullname= trim($_POST['full_name']);
		$email = htmlentities($_POST['email']);
		$username = trim($_POST['username']);
		$password = $_POST['password'];
		$confirm_password =  $_POST['confirm_password'];
		$error_code=NULL;
		$error_desc=NULL;
		$min=5;
		if(strlen($fullname)<$min){
			$error_desc= "Full name should be of length Greater than ${min}";
			$error_code = '1';
		}
		elseif(strlen($username)<$min){
			$error_desc= "Username should be of length Greater than ${min}";
			$error_code = '2';
		}
		elseif(!preg_match("/^[a-z0-9]{3,30}$/i", $username)) 
			{
				$error_desc="Username should be A-Z, 0-9 and ${min}-30 characters long";
				$error_code="3";
			}
		elseif(strlen($email)<10){
			$error_desc= "Email should be of length Greater than 10";
			$error_code = '4';	
		}
		elseif($password != $confirm_password){
			$error_desc= "Password Mismatched type again";
			$error_code = '5';	
		}
		else{
		$validation_code ='';
		$output= Helper::insertUsers($fullname, $username, $email, $password, $validation_code);
		if($output=='success'){
			$error_desc = "Succefully Registered With Aevitas Now Goto <b>Login</>";
			$error_code = '6';
		}
		if($output=='username_exists'){
			$error_desc="Username already exists";
			$error_code='2';
		}
		if($output=='email_exists'){
			$error_desc="Email already exists";
			$error_code='4';
		}
		if($output=='error'){
			$error_desc="Some Unexpected Error occured ! -_-";
			$error_code='9';
		}
	}
	}

	?>
<br><br>
<div class="container">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3 col-center">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-6">
								<a href="login.php">Login</a>
							</div>
							<div class="col-6">
								<a href="register.php" class="active" id="">Register</a>
							</div>
						</div>
						<hr>
					</div>
					
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="register-form" method="post" role="form" >
									<div class="form-group">
										<input type="text" name="full_name" id="full_name" tabindex="1" class="form-control" placeholder="Full name" value="" required >
										<?php if(isset($error_code)&& $error_code=='1'&&$error_desc){echo '<div class="error-msg">'.$error_desc.'</div>';}?>
									</div>
									<div class="form-group">
										<input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="" required >
										<?php if(isset($error_code)&& $error_code=='2'&&$error_desc){echo '<div class="error-msg">'.$error_desc.'</div>';}?>
										<?php if(isset($error_code)&& $error_code=='3'&&$error_desc){echo '<div class="error-msg">'.$error_desc.'</div>';}?>
										
									</div>
									<div class="form-group">
										<input type="email" name="email" id="register_email" tabindex="1" class="form-control" placeholder="Email Address" value="" required >
										<?php if(isset($error_code)&& $error_code=='4'&&$error_desc){echo '<div class="error-msg">'.$error_desc.'</div>';}?>
									</div>
									<div class="form-group">
										<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" required>
									</div>
									<div class="form-group">
										<input type="password" name="confirm_password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password" required>
										<?php if(isset($error_code)&& $error_code=='5'&&$error_desc){echo '<div class="error-msg">'.$error_desc.'</div>';}?>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
											</div>
										</div>
									</div>
									<?php if(isset($error_code)&& $error_code=='6'&&$error_desc){echo '<div class="success-msg">'.$error_desc.'</div>';}?>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><br><br>
<script type="text/javascript" src="./js/mine.js"></script>
<?php include $_SERVER['DOCUMENT_ROOT'].'/include/footer.php';?>