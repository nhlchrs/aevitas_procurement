<?php 
include $_SERVER['DOCUMENT_ROOT']."/core/db.php";
include $_SERVER['DOCUMENT_ROOT']."/helper/searchHelper.php";
if(isset($_POST['searchTerm'])){
	$search =  $_POST['searchTerm'];
	$searchHelper = new searchHelper();
	$searchAnything= $searchHelper->searchWebsite($search);
	if($searchAnything){
		echo '<div class="row">';
		foreach ($searchAnything as $key) {
			echo '<ul><li id="suggestion-search-result" type="none">'.$key->category.'</li></ul>';
		}
			echo '</div>';
		}
	else{
		echo "Nothing Found";
	}
}
?>