<?php 
include $_SERVER['DOCUMENT_ROOT']."/core/db.php"; 
include $_SERVER['DOCUMENT_ROOT']."/helper/productHelper.php";
include $_SERVER['DOCUMENT_ROOT']."/include/session.php";
$product = new productHelper();
if(isset($_GET['subcategory'])){
$specificId= $_GET['subcategory'];
$getfinalProdcutsDetails = $product->getFinalProductDetails($specificId);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="/js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
	<link rel="shortcut icon" type="text/css" href="./images/apspl.jpg">
	<link rel="stylesheet" type="text/css" href="/css/fonts/font-awesome.css">
    <title>Products | Aevitas Procurement Service </title>
</head>

<body>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/navbar.first.php";
include $_SERVER['DOCUMENT_ROOT']."/include/navbar.php"; ?>
<div class="row">
		<div class="col-md-12 col-lg-12 col-xs-12">
			<img  src="./images/aboutvendor.jpg" class="aa" style="opacity:1;">
			<h1 class="imgsetting">Our Products </h1>
			<span class="imgsettings"> <i class="fa fa-home" aria-hidden="true"> &gt; Product</i></span>
		</div>
	</div>
	<div class="row text-center">
	<?php 
		foreach ($getfinalProdcutsDetails as $key) {
	$finalProducts = $key->finalproductsName;	
	$images= $key->finalproductsImages;
		echo '<div class="col-md-6 col-lg-3 col-sm-12 padding-t20 ">';
			echo '<div class="final-gradient-background-color">';
			if(isset($_SESSION['id']) && $_SESSION['is_admin'] && $_SESSION['is_admin']=='Y'){
		echo "<input type='hidden' id='adminOrUser' rel=".$_SESSION['is_admin'].">";
	}
		echo '<img class="zoom" style="width:200px; height:200px;" src="data:image/jpg;base64,'.base64_encode($images).'">';
			echo '<p class="zoom">'.ucfirst($finalProducts).'</p>';
			echo '</div>';
		echo '</div>';
}	
}else{
	header('location:/ 	.php');
}
?>
</div>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/footer.php"; ?>
</body>
</html>