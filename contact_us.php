<?php 
include $_SERVER['DOCUMENT_ROOT']."/core/db.php"; 
include $_SERVER['DOCUMENT_ROOT']."/include/session.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="./js/jquery.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <link rel="shortcut icon" type="text/css" href="./images/apspl.jpg">
    <link rel="stylesheet" type="text/css" href="css/fonts/font-awesome.css">
    <title>Contact us | Aevitas Procurement Service </title>
</head>

<body>

<?php include "./include/navbar.first.php";
include "./include/navbar.php";
?>
<div class="row">
        <div class="col-md-12 col-lg-12 col-xs-12 ">
            <img src="./images/aboutvendor.jpg" class="aa" style="opacity:1;">
            <h1 class="imgsetting">Contact Us </h1>
            <span class="imgsettings"> <i class="fa fa-home" aria-hidden="true"> &gt; Contact Us</i></span>
        </div>
    </div>
<div class="container" style="margin-top: 30px;">
<div id="contact-form">
        <div class="row">  
            <div class="col-md-6 col-lg-6 col-sm-6">
                <div class="form-group">
                    <label for="form_name">Fullname </label>
                    <input id="fullname" type="text" name="firstname" class="form-control" placeholder="Please enter your firstname *" required="required" data-error="Firstname is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-sm-6">
                <div class="form-group">
                    <label for="form_lastname">Phone</label>
                    <input id="userPhone" type="number" name="lastname" class="form-control" placeholder="Please enter your lastname *" required="required" data-error="Lastname is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="form_email">Email </label>
                    <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="true" data-error="Valid email is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="form_need">Please specify your need </label>
                    <select id="requestType" name="need" class="form-control" required="required" data-error="Please specify your need.">
                        <option value="Request quotation">Request quotation</option>
                        <option value="Request order status">Request order status</option>
                        <option value="Request copy of an invoice">Request copy of an invoice</option>
                        <option value="Other">Other</option>
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="form_message">Message </label>
                    <textarea id="form_message" name="message" class="form-control" placeholder="Message for me *" rows="4" required="required" data-error="Please, leave us a message."></textarea>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-12">
                <input type="button" name="submit" class="btn btn-success" id="contact_us" value="Send message">
            </div>
        </div><br><br>
        <div class="row">
            <div class="col-md-12">
                <div id="error" class="bg bg-danger" hidden="true"></div>
            </div>
        </div>
    </div><br><br>

</div>
<?php include $_SERVER['DOCUMENT_ROOT']."/include/footer.php"; ?>
</body>
</html>
